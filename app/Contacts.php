<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contacts extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    protected $fillable =['name', 'email', 'phone_number', 'subject', 'message', 'status', 'updated-by'];
}
