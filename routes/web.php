<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

// Route::get('/contact', function () {
//     return view('contact');
// });

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'namespace' => 'Frontend',
    'prefix' => 'contact',
    'as' => 'contact.'
], function() {

    Route::get('/', 'ContactController@index')->name('index');
    Route::get('/confirm', 'ContactController@confirm')->name('confirm');
    Route::get('/success', 'ContactController@success')->name('success');

    Route::post('/pre-confirm', 'ContactController@preConfirm')->name('pre-confirm');
    Route::post('/store', 'ContactController@store')->name('store');
}); 

Route::group([
    'namespace' => 'Backend',
    'prefix' => 'admin',
    'as' => 'admin.contact.',
    'middleware'=> 'auth'
], function() {
    Route::get('/contact', 'ContactController@index')->name('index');
    Route::get('/contact/{any}', 'ContactController@show')->name('show');
    Route::put('/contact/{any}', 'ContactController@put')->name('put');

});