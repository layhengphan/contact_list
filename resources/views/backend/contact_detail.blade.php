
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pb-4">
                <form action="{{route('admin.contact.put',$contact->id)}}" method="post">
                    @method('PUT')
                    @csrf

                    <button type="submit" class="@if($contact->status =='pending') btn-success @endif btn border border-secondary float-right ml-2"
                            name="status" value="pending">Pending</button>
                    
                    <button type="submit" class="@if($contact->status =='in_progress') btn-success @endif btn border border-secondary float-right ml-2"
                            name="status" value="in_progress">In progress</button>
                    
                    <button type="submit" class="@if($contact->status =='completed') btn-success @endif btn border border-secondary float-right ml-2"
                            name="status" value="completed">Completed</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <table class="table table-bordered">
                    <tr>
                        <th scope="row">Name</th>
                        <td>{{$contact->name}}</td>
                    </tr>

                    <tr>
                        <th scope="row">Email</th>
                        <td>{{$contact->email}}</td>
                    </tr>

                    <tr>
                        <th scope="row">Phone Number</th>
                        <td>{{$contact->phone_number}}</td>
                    </tr>

                    <tr>
                        <th scope="row">Subject</th>
                        <td>{{$contact->subject}}</td>
                    </tr>

                    <tr>
                        <th scope="row" style="...">Message</th>
                        <td><p>{{$contact->message}}</p></td>
                    </tr>
            </table>
        </div>
    </div>
</div>
@endsection